<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{nicechat}prestashop>nicechat_49b780e8aac36dc9b21e1792b1476464'] = 'Nice Chat';
$_MODULE['<{nicechat}prestashop>nicechat_b04758a565e0f9409c57cc4b98f8d5b0'] = 'Модуль для сервиса Nice Chat';
$_MODULE['<{nicechat}prestashop>nicechat_876f23178c29dc2552c0b48bf23cd9bd'] = 'Вы уверены, что хотите удалить модуль?';
$_MODULE['<{nicechat}prestashop>nicechat_0f40e8817b005044250943f57a21c5e7'] = 'Имя не указано';
$_MODULE['<{nicechat}prestashop>nicechat_fe5d926454b6a8144efce13a44d019ba'] = 'Неправильное значение';
$_MODULE['<{nicechat}prestashop>nicechat_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки обновлены';
$_MODULE['<{nicechat}prestashop>nicechat_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{nicechat}prestashop>nicechat_55690b67faad1be465fab3a37ab6c642'] = 'ID партнера';
$_MODULE['<{nicechat}prestashop>nicechat_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{nicechat}prestashop>nicechat_630f6dc397fe74e52d5189e2c80f282b'] = 'Вернуться к списку';
