<!-- MODULE NICE CHAT -->
<!--
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 *  @author    Nice Chat
 *  @copyright 2017 WORLD TECH OÜ
 *  @license   LICENSE.txt
 */
-->
<script src="https://widget.nice.chat/widget/widget.js{if $partnerId}?partner_id={$partnerId}{/if}"></script>
<!-- /MODULE NICE CHAT -->